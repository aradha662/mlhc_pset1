import os
import sys
import numpy as np
import pandas
import csv
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.metrics import roc_auc_score, roc_curve
from sklearn.feature_extraction.text import CountVectorizer
import matplotlib.pyplot as plt


feature_black_list = ['mort_hosp',
                      'mort_oneyr']

stopwords =  set(['a', 'about', 'above', 'after', 'again', 'against', 'all', 'am', 'an', 'and',
                 'any', 'are', 'as', 'at', 'be', 'because', 'been', 'before', 'being', 'below',
                 'between', 'both', 'but', 'by', 'can', 'did', 'do', 'does', 'doing', 'don',
                 'down', 'during', 'each', 'few', 'for', 'from', 'further', 'had', 'has', 'have',
                 'having', 'he', 'her', 'here', 'hers', 'herself', 'him', 'himself', 'his', 'how',
                 'i', 'if', 'in', 'into', 'is', 'it', 'its', 'itself', 'just', 'me', 'more', 'most',
                 'my', 'myself', 'no', 'nor', 'not', 'now', 'of', 'off', 'on', 'once', 'only', 'or',
                 'other', 'our', 'ours', 'ourselves', 'out', 'over', 'own', 's', 'same', 'she', 'should',
                 'so', 'some', 'such', 't', 'than', 'that', 'the', 'their', 'theirs', 'them', 'themselves',
                 'then', 'there', 'these', 'they', 'this', 'those', 'through', 'to', 'too', 'under',
                 'until', 'up', 'very', 'was', 'we', 'were', 'what', 'when', 'where', 'which', 'while',
                 'who', 'whom', 'why', 'will', 'with', 'yo', 'your', 'yours', 'yourself', 'yourselves'])

def read_ehr_csv(fname):
    with open(fname, 'r') as f:
        reader = csv.reader(f)
        header_idx = True
        train_features = []
        val_features = []
        train_labels = []
        val_labels = []
        for row in reader:
            if header_idx:
                header_idx = False
                header = row
            else:
                new_features = []
                label = -1
                for idx in range(len(row) - 1):
                    item = row[idx]
                    if header[idx] in feature_black_list:
                        continue
                    elif header[idx] == 'mort_icu':
                        label = int(item)
                    elif '.' in item:
                        new_features.append(float(item))
                    else:
                        new_features.append(int(item))
                if int(row[-1]) == 1:
                    train_features.append(new_features)
                    train_labels.append(label)
                else:
                    val_features.append(new_features)
                    val_labels.append(label)
        header = header[0:4] + header[7:-1]
        
    return (train_features, train_labels), (val_features, val_labels), header

def create_bow(text):
    text = text.lower().split(" ")
    bow = set([])
    for word in text:
        word = word.strip()
        if word.isalpha() and word not in stopwords:
            bow.add("w_" + word)
    bow = list(bow)
    #tokens = sent_tokenize(text)
    #print(tokens)
    return " ".join(bow)

def read_csv(fname):
    with open(fname, 'r') as f:
        train_features = []
        val_features = []
        train_labels = []
        val_labels = []
        df = pandas.read_csv(fname)
        table = df.values
        header = list(df.columns)[:4]
        
        idx = 0
        print(table.shape)
        for row in table:
            idx += 1
            if idx % 100 == 0:
                print(idx)
            new_features = []
            new_features.append(int(row[0]))
            new_features.append(int(row[1]))
            new_features.append(int(row[2]))
            new_features.append(create_bow(row[3]))
            if int(row[4]) == 1:
                train_features.append(new_features)
                train_labels.append(int(row[6]))
            else:
                val_features.append(new_features)
                val_labels.append(int(row[6]))
                                
                
    return (train_features, train_labels), (val_features, val_labels), header


def scale_features(train_features, val_features, header):

    minmax = MinMaxScaler()
    vectorizer = CountVectorizer()

    train_word_features = []
    float_train_features = []
    boolean_train_features = []
    for feature in train_features:
        boolean_train_features.append(feature[4:16])
        float_train_features.append(feature[0:4] + feature[16:-1])
        train_word_features.append(feature[-1])
    train_word_features = vectorizer.fit_transform(train_word_features).toarray()
    float_train_features = minmax.fit_transform(float_train_features)
    new_train_features = np.concatenate((float_train_features,
                                         boolean_train_features,
                                         train_word_features), 1)

    val_word_features = []
    float_val_features = []
    boolean_val_features = []
    for feature in val_features:
        boolean_val_features.append(feature[4:16])
        float_val_features.append(feature[0:4] + feature[16:-1])
        val_word_features.append(feature[-1])
    val_word_features = vectorizer.transform(val_word_features).toarray()
    float_val_features = minmax.transform(float_val_features)
    new_val_features = np.concatenate((float_val_features,
                                       boolean_val_features,
                                       val_word_features), 1)

    feature_names = header[0:4] + header[16:-1] + header[4:16] + vectorizer.get_feature_names()
    return new_train_features, new_val_features, feature_names


def link_features(train_1, train_2):
    t_ids_1 = {}
    t_features_1, t_labels_1 = train_1
    t_features_2, t_labels_2 = train_2

    for idx in range(len(t_features_1)):
        feature = t_features_1[idx]
        label = t_labels_1[idx]
        key = str(feature[0]) + str(feature[1]) + str(feature[2]) + str(label)
        if key not in t_ids_1:
            t_ids_1[key] = (feature, label)

    print(len(t_ids_1))
    print(len(t_features_1))
    t_ids_2 = {}
    for idx in range(len(t_features_2)):
        feature = t_features_2[idx]
        label = t_labels_2[idx]
        key = str(feature[0]) + str(feature[1]) + str(feature[2]) + str(label)
        if key not in t_ids_2:
            t_ids_2[key] = (feature[3:], label)    

    print(len(t_ids_2))
    print(len(t_features_2))            

    new_train_features = []
    new_train_labels = []
    for key in t_ids_1:
        if key in t_ids_2:
            f1, l = t_ids_1[key]
            f2, l = t_ids_2[key]
            new_train_features.append(f1 + f2)
            new_train_labels.append(l)

    return new_train_features, new_train_labels

def main(ehr_fname, notes_fname):
    print("Generating features")
    train_info, val_info, header = read_csv(notes_fname)
    train_features, train_labels = train_info
    val_features, val_labels = val_info

    ehr_train_info, ehr_val_info, ehr_header = read_ehr_csv(ehr_fname)
    ehr_train_features, ehr_train_labels = ehr_train_info
    ehr_val_features, ehr_val_labels = ehr_val_info

    train_features, train_labels = link_features(ehr_train_info,
                                                 train_info)
    val_features, val_labels = link_features(ehr_val_info,
                                             val_info)
    
    feature_names = ehr_header + header[3:]

    print("Scaling features")
    
    train_features, val_features, feature_names = scale_features(train_features,
                                                                 val_features,
                                                                 feature_names)

    print(len(train_features), len(val_features))
    model = LogisticRegression(#class_weight='balanced',
                               penalty='l1',
                               solver='liblinear',
                               max_iter=1000)
    
    model.fit(train_features, train_labels)
    print(model.score(train_features, train_labels))
    print(model.score(val_features, val_labels))
    print(model.coef_[0].shape)
    fpr, tpr, thresholds = roc_curve(val_labels, model.decision_function(val_features))
    auroc = roc_auc_score(val_labels, model.decision_function(val_features))

    coeff_vals = sorted(model.coef_[0], reverse=True)
    
    coeff = model.coef_[0].argsort()[::-1]
    #print(header)
    feature_names = np.array(feature_names)
    print(coeff_vals[:6])
    top_5_best = feature_names[coeff[:6]]
    print(coeff_vals[-5:])
    top_5_worst = feature_names[coeff[-5:]]
    print(top_5_best)
    print(top_5_worst)
    plt.plot(fpr, tpr, label='ROC curve (area = %0.2f)' % auroc)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    plt.show()


args = sys.argv
ehr_fname = args[1]
notes_fname = args[2]

if __name__ == "__main__":
    main(ehr_fname, notes_fname)
    
