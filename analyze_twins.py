import os
import sys
import numpy as np
import pandas
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.metrics import roc_auc_score, roc_curve
import matplotlib.pyplot as plt


def read_csv(fname):
    df = pandas.read_csv(fname)
    print(df.columns)
    header = list(df.columns)
    twin_ids = {}
    table = df.values
    ate = 0.0
    count = 0.0
    threshold = 2700
    print(len(table))

    for idx in range(len(table)):
        if idx % 100 == 0:
            print(idx)

        twin_id = df.loc[idx, 'pair_id']
        if twin_id not in twin_ids:
            twin_ids[twin_id] = (df.loc[idx, 'mort'],
                                 df.loc[idx, 'dbirwt'])
                                 
            
        else:
            mort_1, bw_1 = twin_ids[twin_id]
            mort_2, bw_2 = (df.loc[idx, 'mort'],
                            df.loc[idx,'dbirwt'])
            if bw_1 < 2700 and bw_2 >= 2700:
                ate += mort_1 - mort_2
                count += 1
            elif bw_1 >= 2700 and bw_2 < 2700:
                ate += mort_2 - mort_1
                count += 1

    print(ate / count)


def main(fname):
    read_csv(fname)

    
args = sys.argv
fname = args[1]

if __name__ == "__main__":
    main(fname)
    
