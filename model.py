import os
import sys
import numpy as np
import csv
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.metrics import roc_auc_score, roc_curve
import matplotlib.pyplot as plt


feature_black_list = ['mort_hosp',
                      'mort_oneyr']

def read_csv(fname):
    with open(fname, 'r') as f:
        reader = csv.reader(f)
        header_idx = True
        train_features = []
        val_features = []
        train_labels = []
        val_labels = []
        for row in reader:
            if header_idx:
                header_idx = False
                header = row
            else:
                new_features = []
                label = -1
                for idx in range(len(row) - 1):
                    item = row[idx]
                    if header[idx] in feature_black_list:
                        continue
                    elif header[idx] == 'mort_icu':
                        label = int(item)
                    elif '.' in item:
                        new_features.append(float(item))
                    else:
                        new_features.append(int(item))
                if int(row[-1]) == 1:
                    train_features.append(new_features)
                    train_labels.append(label)
                else:
                    val_features.append(new_features)
                    val_labels.append(label)

    return (train_features, train_labels), (val_features, val_labels), header


def scale_features(train_features, val_features):
    scaler = StandardScaler()
    minmax = MinMaxScaler()
    float_train_features = []
    boolean_train_features = []
    for feature in train_features:
        boolean_train_features.append(feature[4:16])
        float_train_features.append(feature[0:4] + feature[16:])

    #float_train_features = scaler.fit_transform(float_train_features)
    float_train_features = minmax.fit_transform(float_train_features)
    boolean_train_features = np.array(boolean_train_features)

    new_train_features = np.concatenate((float_train_features, boolean_train_features), 1)

    float_val_features = []
    boolean_val_features = []
    for feature in val_features:
        boolean_val_features.append(feature[4:16])
        float_val_features.append(feature[0:4] + feature[16:])

    #float_val_features = scaler.transform(float_val_features)
    float_val_features = minmax.transform(float_val_features)    
    boolean_val_features = np.array(boolean_val_features)

    new_val_features = np.concatenate((float_val_features, boolean_val_features), 1)

    return new_train_features, new_val_features


def main(fname):
    train_info, val_info, header = read_csv(fname)
    train_features, train_labels = train_info
    val_features, val_labels = val_info

    train_features, val_features = scale_features(train_features,
                                                  val_features)

    header = header[0:4] + header[19:-1] +  header[7:19]
    model = LogisticRegression(#class_weight='balanced',
                               solver='lbfgs',
                               max_iter=1000)
    model.fit(train_features, train_labels)
    print(model.score(train_features, train_labels))
    print(model.score(val_features, val_labels))
    print(model.coef_, model.coef_[0].shape)
    fpr, tpr, thresholds = roc_curve(val_labels, model.decision_function(val_features))
    auroc = roc_auc_score(val_labels, model.decision_function(val_features))

    coeff = model.coef_[0].argsort()[::-1]
    print(header)
    header = np.array(header)
    print(model.coef_[0][coeff[:6]])
    top_5_best = header[coeff[:6]]
    print(model.coef_[0][coeff[-5:]])
    top_5_worst = header[coeff[-5:]]
    print(top_5_best)
    print(top_5_worst)
    plt.plot(fpr, tpr, label='ROC curve (area = %0.2f)' % auroc)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    plt.show()

    
args = sys.argv
fname = args[1]

if __name__ == "__main__":
    main(fname)
    
