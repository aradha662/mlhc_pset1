import os
import sys
import numpy as np
import pandas
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.metrics import roc_auc_score, roc_curve
from sklearn.feature_extraction.text import CountVectorizer
import matplotlib.pyplot as plt


feature_black_list = ['mort_hosp',
                      'mort_oneyr']

stopwords =  set(['a', 'about', 'above', 'after', 'again', 'against', 'all', 'am', 'an', 'and',
                 'any', 'are', 'as', 'at', 'be', 'because', 'been', 'before', 'being', 'below',
                 'between', 'both', 'but', 'by', 'can', 'did', 'do', 'does', 'doing', 'don',
                 'down', 'during', 'each', 'few', 'for', 'from', 'further', 'had', 'has', 'have',
                 'having', 'he', 'her', 'here', 'hers', 'herself', 'him', 'himself', 'his', 'how',
                 'i', 'if', 'in', 'into', 'is', 'it', 'its', 'itself', 'just', 'me', 'more', 'most',
                 'my', 'myself', 'no', 'nor', 'not', 'now', 'of', 'off', 'on', 'once', 'only', 'or',
                 'other', 'our', 'ours', 'ourselves', 'out', 'over', 'own', 's', 'same', 'she', 'should',
                 'so', 'some', 'such', 't', 'than', 'that', 'the', 'their', 'theirs', 'them', 'themselves',
                 'then', 'there', 'these', 'they', 'this', 'those', 'through', 'to', 'too', 'under',
                 'until', 'up', 'very', 'was', 'we', 'were', 'what', 'when', 'where', 'which', 'while',
                 'who', 'whom', 'why', 'will', 'with', 'yo', 'your', 'yours', 'yourself', 'yourselves'])

def create_bow(text):
    text = text.lower().split(" ")
    bow = set([])
    for word in text:
        word = word.strip()
        if word.isalpha() and word not in stopwords:
            bow.add(word)
            #bow.append(word)
    bow = list(bow)
    #tokens = sent_tokenize(text)
    #print(tokens)
    return " ".join(bow)

def read_csv(fname):
    with open(fname, 'r') as f:
        train_features = []
        val_features = []
        train_labels = []
        val_labels = []
        df = pandas.read_csv(fname)
        table = df.values
        header = list(df.columns)[:4]
        
        idx = 0
        print(table.shape)
        for row in table:
            idx += 1
            if idx % 100 == 0:
                print(idx)
            new_features = []
            new_features.append(int(row[0]))
            new_features.append(int(row[1]))
            new_features.append(int(row[2]))
            new_features.append(create_bow(row[3]))
            if int(row[4]) == 1:
                train_features.append(new_features)
                train_labels.append(int(row[6]))
            else:
                val_features.append(new_features)
                val_labels.append(int(row[6]))
                                
                
    return (train_features, train_labels), (val_features, val_labels), header


def scale_features(train_features, val_features, header):

    minmax = MinMaxScaler()
    vectorizer = CountVectorizer()

    train_word_features = []
    train_num_features = []
    for feature in train_features:
        train_num_features.append(feature[0:3])
        train_word_features.append(feature[3])
    train_word_features = vectorizer.fit_transform(train_word_features).toarray()
    train_num_features = minmax.fit_transform(train_num_features)
    new_train_features = np.concatenate((train_num_features,
                                         train_word_features), 1)
    #new_train_features = minmax.fit_transform(new_train_features)    
    val_word_features = []
    val_num_features = []
    for feature in val_features:
        val_num_features.append(feature[0:3])
        val_word_features.append(feature[3])
    val_word_features = vectorizer.transform(val_word_features).toarray()
    val_num_features = minmax.transform(val_num_features)
    new_val_features = np.concatenate((val_num_features,
                                       val_word_features), 1)
    #new_val_features = minmax.transform(new_val_features)
    feature_names = header[:3] + vectorizer.get_feature_names()
    return new_train_features, new_val_features, feature_names


def main(fname):
    print("Generating features")
    train_info, val_info, header = read_csv(fname)
    train_features, train_labels = train_info
    val_features, val_labels = val_info

    print("Scaling features")
    train_features, val_features, feature_names = scale_features(train_features,
                                                                 val_features,
                                                                 header)


    model = LogisticRegression(#class_weight='balanced',
                               penalty='l1',
                               solver='liblinear',
                               max_iter=1000)
    
    model.fit(train_features, train_labels)
    print(model.score(train_features, train_labels))
    print(model.score(val_features, val_labels))
    print(model.coef_[0].shape)
    fpr, tpr, thresholds = roc_curve(val_labels, model.decision_function(val_features))
    auroc = roc_auc_score(val_labels, model.decision_function(val_features))

    coeff_vals = sorted(model.coef_[0], reverse=True)
    
    coeff = model.coef_[0].argsort()[::-1]
    #print(header)
    feature_names = np.array(feature_names)
    print(coeff_vals[:6])
    top_5_best = feature_names[coeff[:6]]
    print(coeff_vals[-5:])
    top_5_worst = feature_names[coeff[-5:]]
    print(top_5_best)
    print(top_5_worst)
    plt.plot(fpr, tpr, label='ROC curve (area = %0.2f)' % auroc)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    plt.show()

    
args = sys.argv
fname = args[1]

if __name__ == "__main__":
    main(fname)
    
