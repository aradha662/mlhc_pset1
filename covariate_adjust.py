import os
import sys
import numpy as np
import pandas
from copy import deepcopy
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.svm import SVC, LinearSVC, SVR
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.metrics import roc_auc_score, roc_curve
import matplotlib.pyplot as plt


def read_csv(fname):
    df = pandas.read_csv(fname)
    #print(df.columns)
    header = list(df.columns)
    table = df.values
    print(len(table))

    print(df.corr()['tobacco'])

    col_names = ['mort',
                 'csex',
                 'dmage',
                 'mrace',
                 'dmeduc',
                 'dmar',
                 'dlivord',
                 'mpcb',
                 'anemia',
                 'cardiac',
                 'lung',
                 'diabetes',
                 'herpes',
                 'hydra',
                 'hemo',
                 'chyper',
                 'phyper',
                 'eclamp',
                 'incervix',
                 'pre4000',
                 'preterm',
                 'renal',
                 'rh',
                 'uterine',
                 'othermr',
                 'tobacco',
                 'alcohol',
                 'dfage',
                 'frace',
                 'dfeduc',
                 'infant_id',
                 'term'
                 ]
    features = []
    targets = []
    df['mrace'] = df.mrace.astype('category').cat.rename_categories(range(1, df.mrace.nunique()+1))
    df['frace'] = df.frace.astype('category').cat.rename_categories(range(1, df.frace.nunique()+1))
    df['dmeduc'] = df.dmeduc.astype('category').cat.rename_categories(range(1, df.dmeduc.nunique()+1))
    df['dfeduc'] = df.dfeduc.astype('category').cat.rename_categories(range(1, df.dfeduc.nunique()+1))
    
    for idx in range(len(table)):
        if idx % 1000 == 0 and idx != 0:
            print(idx)

        features.append(df.loc[idx, col_names])
        target = df.loc[idx, 'dbirwt']
        targets.append(target)

    return features, targets, col_names


def compute_ate(model, features, col_names):
    ate = 0.0
    
    for feature in features:
        
        used_tobacco = feature['tobacco']
        if used_tobacco == 0 :
            counterfactual = deepcopy(feature)
            counterfactual['tobacco'] = 1
            feature = np.array(feature).reshape((1, -1))
            counterfactual = np.array(counterfactual).reshape((1,-1))
            ate += model.predict(counterfactual)[0] - model.predict(feature)[0]
        else:
            counterfactual = deepcopy(feature)
            counterfactual['tobacco'] = 0
            feature = np.array(feature).reshape((1, -1))
            counterfactual = np.array(counterfactual).reshape((1,-1))
            
            ate += model.predict(feature)[0] - model.predict(counterfactual)[0]

    return ate / len(features)


def propensity_score(features, targets, col_names):
    #model = SVC(probability=True)
    model = LogisticRegression(solver='liblinear',
                               max_iter=1000)
                               
    prop_features = []
    prop_labels = []
    for feature in features:
        prop_feature = []
        for name in col_names:
            if name == 'tobacco':
                prop_labels.append(feature[name])
            else:
                prop_feature.append(feature[name])
        prop_features.append(prop_feature)

    model = model.fit(prop_features, prop_labels)
    print(model.score(prop_features, prop_labels))

    ate = 0.0


    prop_1_scores = []
    prop_0_scores = []
    for idx in range(len(prop_features)):
        prop_feature = np.array(prop_features[idx]).reshape((1,-1))
        prop_label = prop_labels[idx]
        bw = targets[idx]
        prob = model.predict_proba(prop_feature)[0][prop_label]
        if prop_label == 1:
            ate += bw / prob
            prop_1_scores.append(prob)
        else:
            ate -= bw / prob
            prop_0_scores.append(prob)

            
    plt.plot([1] * len(prop_1_scores), prop_1_scores, 'ro')
    plt.plot([0] * len(prop_0_scores), prop_0_scores, 'bo')
    plt.ylim([0.0, 1.05])
    plt.xlabel('T')
    plt.ylabel('Probability of T | x')
    plt.title('Distribution of Probabilities')
    plt.show()
    plt.hist(prop_1_scores + prop_0_scores, 50)
    plt.xlabel('Propensity Score')
    plt.ylabel('Number of Examples')
    plt.title('Distribution of Propensity Scores')    
    plt.show()
    print(len(prop_1_scores), len(prop_0_scores))
          
    print(np.mean(np.array(prop_1_scores)))
    print(np.var(np.array(prop_1_scores)))
    print(np.mean(np.array(prop_0_scores)))
    print(np.var(np.array(prop_0_scores)))

    print( ate / len(features))
    
def main(fname):
    features, targets, header = read_csv(fname)

    propensity_score(features, targets, header)
    

    """
    model = LinearRegression()
    model = model.fit(np.array(features), np.array(targets))
    print(header)
    print("SCORE: ", model.score(np.array(features), np.array(targets)))
    #print(model.coef_)

    ate = compute_ate(model, features, header)
    print(ate)
    """
args = sys.argv
fname = args[1]

if __name__ == "__main__":
    main(fname)
    
